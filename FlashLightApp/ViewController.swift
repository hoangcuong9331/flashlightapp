//
//  ViewController.swift
//  FlashLightApp
//
//  Created by Le Nguyen Hoang Cuong on 10/12/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var SliderAlpha: UISlider!
    @IBOutlet weak var TxtAlpha: UITextField!
    @IBOutlet weak var SliderGreen: UISlider!
    @IBOutlet weak var TxtGreen: UITextField!
    @IBOutlet weak var SliderBlue: UISlider!
    @IBOutlet weak var TxtBlue: UITextField!
    @IBOutlet weak var SliderRed: UISlider!
    @IBOutlet weak var TxtRed: UITextField!
    @IBOutlet weak var View1: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Them nut done
        
        UI.addDoneButtonForTextField(textField: TxtRed)
        UI.addDoneButtonForTextField(textField: TxtGreen)
        UI.addDoneButtonForTextField(textField: TxtBlue)
        UI.addDoneButtonForTextField(textField: TxtAlpha)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "SegueShowFlashLight"{
            
            let destination: FlashLightController = segue.destination as! FlashLightController
            destination.Color = GetColor()
        }
    }
    
    @IBAction func TxtAlpha_Edittingchanged(_ sender: Any) {
        if(TxtAlpha.text != ""){
            SliderAlpha.setValue(Float(TxtAlpha.text!)!/255, animated: true)
            View1.backgroundColor = GetColor()
        }
    }
    @IBAction func TxtGreen_Edittingchanged(_ sender: Any) {
        if(TxtGreen.text != ""){
            SliderGreen.setValue(Float(TxtGreen.text!)!/255, animated: true)
            View1.backgroundColor = GetColor()
        }
    }
    @IBAction func TxtBlue_Edittingchanged(_ sender: Any) {
        if(TxtBlue.text != ""){
            SliderBlue.setValue(Float(TxtBlue.text!)!/255, animated: true)
            View1.backgroundColor = GetColor()
        }
    }
    @IBAction func TxtRed_Edittingchanged(_ sender: Any) {
        if (TxtRed.text != ""){
            SliderRed.setValue(Float(TxtRed.text!)!/255, animated: true)
            View1.backgroundColor = GetColor()
        }
    }
    @IBAction func SilderAlpha_Valuechanged(_ sender: UISlider) {
        TxtAlpha.text = String(Int(SliderAlpha.value*255))
        View1.backgroundColor = GetColor()
    }
    @IBAction func SliderGreen_Valuechanged(_ sender: UISlider) {
        TxtGreen.text = String(Int(SliderGreen.value*255))
        View1.backgroundColor = GetColor()
    }
    @IBAction func SliderRed_Valuechanged(_ sender: UISlider) {
        TxtRed.text = String(Int(SliderRed.value*255))
        View1.backgroundColor = GetColor()
    }
    @IBAction func SliderBlue_Valuechanged(_ sender: UISlider) {
        TxtBlue.text = String(Int(SliderBlue.value*255))
        View1.backgroundColor = GetColor()
    }
    
    func GetColor() -> UIColor {
        let red = CGFloat(SliderRed.value)
        let blue = CGFloat(SliderBlue.value)
        let green = CGFloat(SliderGreen.value)
        let alpha = CGFloat(SliderAlpha.value)
        
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }

}

