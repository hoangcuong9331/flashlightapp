//
//  Helper.swift
//  FlashLightApp
//
//  Created by Le Nguyen Hoang Cuong on 10/16/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

class UI {
    static func addDoneButtonForTextField(textField: UITextField){
            let toolbar = UIToolbar()
            toolbar.items = [
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
                UIBarButtonItem(title: "Done", style: .done, target: textField, action: #selector(UITextField.resignFirstResponder))
            ]
            toolbar.sizeToFit()
            textField.inputAccessoryView = toolbar
        }
}
