//
//  FlashLightController.swift
//  FlashLightApp
//
//  Created by Le Nguyen Hoang Cuong on 10/18/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

class FlashLightController: UIViewController {

    @IBOutlet weak var FlashLightView: UIView!
    var Color: UIColor?
    override func viewDidLoad() {
        super.viewDidLoad()
        if Color != nil{
            FlashLightView.backgroundColor = Color        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        self.FlashLightView.addGestureRecognizer(tap)
    }
    @objc func doubleTapped(){
        let ID:String = "MainID"
        let destination = storyboard?.instantiateViewController(withIdentifier: ID) as! ViewController
        self.present(destination, animated: true, completion: nil)
    }
}
